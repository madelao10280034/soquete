package servidor_concurrente;



import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.net.*;
import java.io.*;
//// paquete que contiene las clases de sockets
//...............
////// paquete que contiene las clases para el manejo de flujo de datos
//..........

public class Cliente extends JFrame implements ActionListener{

    Container c;
    JTextField op1,op2;
    JLabel l1,l2,l3;
    JPanel pN,pC,pS;
    JPanel p1,p2,p3,p4;
    JButton boton;
    JTextArea display;
    
    public Cliente()
    {
        setTitle("Cliente de socket");
        setLayout(new GridLayout(4,1));
        c= getContentPane();
        c.setBackground(Color.cyan);
        // creación de las intancias y agregación a los paneles 
//        // de las componentes GUI
//         . . . . . . . .  . . . 
        op1=new JTextField();
                op2=new JTextField();
                l1=new JLabel();
                    l2=new JLabel();    
                        l3=new JLabel();
                    
        pN=new JPanel();
        
           pC=new JPanel();
               pS=new JPanel();
                         p1=new JPanel();
                                   p2=new JPanel();
                                             p3=new JPanel();
                                                       p4=new JPanel();
                                                       boton=new JButton("hacer peticion");
                                                       display=new JTextArea();
        add(op1);
          add(op2);
            add(l1);
              add(l2);
                add(l3);
                  add(pN);
                    add(pC);
                      add(pS);
                        add(p1);
                          add(p2);
                            add(p3);
                              add(p4);
                                add(boton);
                                  add(display);
//                    c.add(pN);
//                    c.add(pC);
//                    c.add(pS);
//                    c.add(p1);
//                    c.add(p2);
//                    c.add(p3);
//                    c.add(p4);
//                    pN.add(op1);
//                    pC.add(op2);
//                    pC.add(op2);
//                    pS.add(l1);
//                    p1.add(l2);
//                    p2.add(l3);
//                    p3.add(boton);
//                    p4.add(display);
                 
            





          pack();
        this.setLocationRelativeTo(null);
        setVisible(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        boton.addActionListener(this);
        
    }         

 @Override
    public void actionPerformed(ActionEvent e) {
       double s1,s2;    
    	s1=Double.parseDouble(op1.getText());
    	s2=Double.parseDouble(op2.getText());
    	System.out.println("Haciendo petición");
    	peticionServidor(s1,s2);
    }
    
 
    
    
     private void peticionServidor(double s1, double s2)
    {
//        // declaración de un objecto para el socket cliente
//    ..........
//         // declaración de los objetos para el flujo de datos
//         .......
//         ........
        Socket client;
        DataInputStream input;
        DataOutputStream output;
         double suma;    
         String Suma;
         try {
//                // creación de la instancia del socket
//                    .............
             client=new Socket(InetAddress.getByName("192.168.152.213"),6000);
             
		    display.setText("Socket Creado....\n");
                    input=new DataInputStream(client.getInputStream());
                    output=new DataOutputStream(client.getOutputStream());
                    
//                // creación de las instancias para el flujo de datos
//                    ............
//                    ...........  
                    
	           display.append("Enviando 1° sumando\n");
                  output.writeDouble(s1);
		     display.append("Enviando 2° sumando\n");
                  output.writeDouble(s2);
		     display.append ("El servidor dice....\n\n");
                  suma=input.readDouble();
                  Suma= String.valueOf (suma);
	           display.append("El  resultado : "+ Suma+"\n\n");
                 display.append("Cerrando cliente\n\n");
                 client.close();
              }

                catch(IOException e){
                 e.printStackTrace();
                }
     }
    
    public static void main(String args[])
    {
        new Cliente();
        
    }
}