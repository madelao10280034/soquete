/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servidor_concurrente;


import java.io.*;
import java.net.*;
import javax.swing.JTextArea;

public class ServidorHilo extends Thread {
     // declaración de instancias globales
     
    JTextArea display;
    Socket conexion;
    ServerSocket server;
    DataOutputStream output;
    DataInputStream input;
     
     double s1,s2,suma;
     int clienteNum;
    
    public ServidorHilo(JTextArea d, ServerSocket s, Socket c,int n)
    {
     display=d;
     server=s;
     conexion=c;
     clienteNum=n;


        System.out.println("se ha entrado al constructor del Hilo cliente: "+ n);
        
    }
    public void run(){
        try {
                display.append("se ha aceptdo una conexion, para el cliente: ");
                display.append("\n Recibiendo sumandos....\n");
                input=new DataInputStream(conexion.getInputStream());
                output=new DataOutputStream(conexion.getOutputStream());
            
                 display.append( "\nRecibiendo el primer sumando\n");
           s1= input.readDouble();
		
	   display.append( "\nRecibiendo el segundo sumando");
           s2 =input.readDouble();
                
           display.append("\nEnviando Resultado..\n");  
           suma= s1+s2;
           output.writeDouble(suma);	   
           display.append( "\n Transmisión terminada.\n Se cierra el socket.\n ");
           conexion.close();
                



       	conexion.close();
         }
      catch(IOException e){
                  e.printStackTrace();
                }

    }
}

